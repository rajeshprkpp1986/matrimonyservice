package com.oc.matimony.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;

import com.oc.matrimony.dao.MatrimonyProfileRepository;
import com.oc.matrimony.entity.MatrimonyProfile;

public class MatrimonyProfileService {

	@Autowired
	private MatrimonyProfileRepository matrimonyProfileRepository;
	
	public List<MatrimonyProfile> getMatrimonyProfiles() {
		
		return matrimonyProfileRepository.findAll();
	}
	
	public Optional<MatrimonyProfile> findMatrimonyProfileById(int id) {
		
		return matrimonyProfileRepository.findById(id);
	}
	
	public MatrimonyProfile addNewMatrimonyProfile(MatrimonyProfile matrimonyProfile) {
		
		return matrimonyProfileRepository.save(matrimonyProfile);
	}
	
	public MatrimonyProfile updateMatrimonyProfile(MatrimonyProfile matrimonyProfile) {
		
		return matrimonyProfileRepository.save(matrimonyProfile);
	}
	
	public void deleteMatrimonyProfile(MatrimonyProfile matrimonyProfile) {
		
		matrimonyProfileRepository.delete(matrimonyProfile);
	}
	
	public void deleteMatrimonyProfileById(int id) {

		matrimonyProfileRepository.deleteById(id);
	}
}
