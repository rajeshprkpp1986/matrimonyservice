package com.oc.matrimony;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MtServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(MtServiceApplication.class, args);
	}

}
