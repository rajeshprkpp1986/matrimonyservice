package com.oc.matrimony.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.oc.matimony.service.MatrimonyProfileService;
import com.oc.matrimony.entity.MatrimonyProfile;

@RestController
@RequestMapping("/matrimonyprofile")
public class MatrimonyProfileController {

	@Autowired
	private MatrimonyProfileService matrimonyProfileService;
	
	@GetMapping
	public ResponseEntity<List<MatrimonyProfile>> getAllProfiles() {
		
		List<MatrimonyProfile> profiles = matrimonyProfileService.getMatrimonyProfiles();
		return new ResponseEntity<List<MatrimonyProfile>> (profiles, new HttpHeaders(), HttpStatus.OK);
	}
	
	@PostMapping("/create")
	public ResponseEntity<MatrimonyProfile> createMatrimonyProfile(MatrimonyProfile profile) {
		
		MatrimonyProfile matrimonyProfile = matrimonyProfileService.addNewMatrimonyProfile(profile);
		return new ResponseEntity<MatrimonyProfile> (matrimonyProfile, new HttpHeaders(), HttpStatus.OK);
	}
}
