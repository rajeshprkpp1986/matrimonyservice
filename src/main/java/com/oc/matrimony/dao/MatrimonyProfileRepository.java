package com.oc.matrimony.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.oc.matrimony.entity.MatrimonyProfile;

@Repository
public interface MatrimonyProfileRepository extends JpaRepository<MatrimonyProfile, Integer> {

}
