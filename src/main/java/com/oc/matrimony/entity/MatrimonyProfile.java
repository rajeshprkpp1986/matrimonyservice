package com.oc.matrimony.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(name="MATRIMONY_PROFILE")
public class MatrimonyProfile {

	@Id
	@GeneratedValue
	private int id;
	
	@Column(name = "MAT_ID")
	private String matId;
	
	@Column(name = "NAME")
	private String name;
	
	@Column(name = "CREATED_FOR")
	private String createdFor;
	
	@Column(name = "DOB")
	private Date dob;
	
	@Column(name = "GENDER")
	private String gender;
	
	@Column(name = "BODY_TYPE")
	private String bodyTpe;
	
	@Column(name = "COMPLEXION")
	private String complexion;
	
	@Column(name = "PHYSICAL_STATUS")
	private String physicalStatus;
	
	@Column(name = "MARTIAL_STATUS")
	private String martialStatus;
	
	@Column(name = "HEIGHT")
	private int height;
	
	@Column(name = "WEIGHT")
	private int weight;
	
	@Column(name = "ABOUT_MY_FAMILY")
	private String abtMyFamily;
	
	@Column(name = "ABOUT_PROFILER")
	private String abtProfiler;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getMatId() {
		return matId;
	}
	public void setMatId(String matId) {
		this.matId = matId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCreatedFor() {
		return createdFor;
	}
	public void setCreatedFor(String createdFor) {
		this.createdFor = createdFor;
	}
	public Date getDob() {
		return dob;
	}
	public void setDob(Date dob) {
		this.dob = dob;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getBodyTpe() {
		return bodyTpe;
	}
	public void setBodyTpe(String bodyTpe) {
		this.bodyTpe = bodyTpe;
	}
	public String getComplexion() {
		return complexion;
	}
	public void setComplexion(String complexion) {
		this.complexion = complexion;
	}
	public String getPhysicalStatus() {
		return physicalStatus;
	}
	public void setPhysicalStatus(String physicalStatus) {
		this.physicalStatus = physicalStatus;
	}
	public String getMartialStatus() {
		return martialStatus;
	}
	public void setMartialStatus(String martialStatus) {
		this.martialStatus = martialStatus;
	}
	public int getHeight() {
		return height;
	}
	public void setHeight(int height) {
		this.height = height;
	}
	public int getWeight() {
		return weight;
	}
	public void setWeight(int weight) {
		this.weight = weight;
	}
	public String getAbtMyFamily() {
		return abtMyFamily;
	}
	public void setAbtMyFamily(String abtMyFamily) {
		this.abtMyFamily = abtMyFamily;
	}
	public String getAbtProfiler() {
		return abtProfiler;
	}
	public void setAbtProfiler(String abtProfiler) {
		this.abtProfiler = abtProfiler;
	}
}
